/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercitos;

/**
 *Classe que armazena as informações de cada personagem.
 * @author Augusto Garcia e Éric Peralta
 */
public class Dados {
    private static int cavAtk = 200, cavVid = 100, cavCust = 100;
    private static int genAtk = 150, genVid = 250, genCust = 500;
    private static int infAtk = 200, infVid = 100, infCust = 150;
    private static int lanAtk = 100, lanVid = 300, lanCust = 50;
    private static int padAtk = 100, padVid = 500, padCust = 350;

    /**
     *Método que retorna ataque de cavaleiro.
     * @return int - retorna o valor do ataque de cavaleiro.
     */
    public static int getCavAtk() {
        return cavAtk;
    }

    /**
     *Método que define o ataque de cavaleiro.
     * @param cavAtk int - novo valor de ataque do cavaleiro.
     */
    public static void setCavAtk(int cavAtk) {
        Dados.cavAtk = cavAtk;
    }

    /**
     *Método que retorna a vida de cavaleiro.
     * @return int - retorna o valor da vida de cavaleiro.
     */
    public static int getCavVid() {
        return cavVid;
    }

    /**
     * Método que define a vida de cavaleiro.
     * @param cavVid int - novo valor da vida de cavaleiro.
     */
    public static void setCavVid(int cavVid) {
        Dados.cavVid = cavVid;
    }

    /**
     *Método que retorna ataque de general.
     * @return int - retorna o valor do ataque de general.
     */
    public static int getGenAtk() {
        return genAtk;
    }

    /**
     *Método que define o ataque de general.
     * @param genAtk int - novo ataque de general
     */
    public static void setGenAtk(int genAtk) {
        Dados.genAtk = genAtk;
    }

    /**
     *Método que retorna a vida de general.
     * @return int - valor da vida de general.
     */
    public static int getGenVid() {
        return genVid;
    }

    /**
     *Método que define a vida de general.
     * @param genVid int - novo valor da vida de general.
     */
    public static void setGenVid(int genVid) {
        Dados.genVid = genVid;
    }

   /**
     *Método que retorna ataque de Infataria.
     * @return int - retorna o valor do ataque de infantaria.
     */
    public static int getInfAtk() {
        return infAtk;
    }

    /**
     * Método que define o ataque da infantaria.
     * @param infAtk int - novo valor de ataque da infantaria.
     */
    public static void setInfAtk(int infAtk) {
        Dados.infAtk = infAtk;
    }

    /**
     * Método que retorna a vida da Infantaria.
     * @return int - valor da vida de infantaira.
     */
    public static int getInfVid() {
        return infVid;
    }

    /**
     * Método que define a vida da Infantaria.
     * @param infVid int - novo valor da vida da infantaria.
     */
    public static void setInfVid(int infVid) {
        Dados.infVid = infVid;
    }

     /**
     *Método que retorna ataque de lanceiro negro.
     * @return int - retorna o valor do ataque de lanceiro negro.
     */
    public static int getLanAtk() {
        return lanAtk;
    }

    /**
     * Método que define o ataque de lanceiro negro.
     * @param lanAtk int - novo valor de ataque de lanceiro negro.
     */
    public static void setLanAtk(int lanAtk) {
        Dados.lanAtk = lanAtk;
    }

    /**
     *Método que retorna a vida de lanceiro negro.
     * @return int - retorna valor da vida de lanceiro negro.
     */
    public static int getLanVid() {
        return lanVid;
    }

    /**
     *Método que define a vida de lanceiro negro.
     * @param lanVid int - novo valor da vidade de lanceiro negro.
     */
    public static void setLanVid(int lanVid) {
        Dados.lanVid = lanVid;
    }

     /**
     *Método que retorna ataque de padre.
     * @return int - retorna o valor do ataque de padre.
     */
    public static int getPadAtk() {
        return padAtk;
    }

    /**
     *Método que define o ataque de padre.
     * @param padAtk int - novo valor do ataque de padre.
     */
    public static void setPadAtk(int padAtk) {
        Dados.padAtk = padAtk;
    }

    /**
     *Método que retorna a vida de padre.
     * @return int - retorna valor da vida de padre.
     */
    public static int getPadVid() {
        return padVid;
    }

    /**
     *Método que define a vida de padre.
     * @param padVid int - novo valor da vida de padre.
     */
    public static void setPadVid(int padVid) {
        Dados.padVid = padVid;
    }
    
    /**
     *Método que retorna o custo de cavaleiro.
     * @return int - retorna o custo de cavaleiro.
     */
    public static int getCavCust() {
        return cavCust;
    }

   /**
     *Método que retorna o custo de general.
     * @return int - retorna o custo de general.
     */
    public static int getGenCust() {
        return genCust;
    }

   /**
     *Método que retorna o custo de infantaria.
     * @return int - retorna o custo de infantaria.
     */
    public static int getInfCust() {
        return infCust;
    }

    /**
     *Método que retorna o custo de lanceiro negro.
     * @return int - retorna o custo de lanceiro negro.
     */
    public static int getLanCust() {
        return lanCust;
    }

    /**
     *Método que retorna o custo de padre.
     * @return int - retorna o custo de padre.
     */
    public static int getPadCust() {
        return padCust;
    }
}
