package Unidades;

/**
 *Classe dos itens.
 * @author Augusto Garcia e Éric Peralta
 */
public class Item {
    private int defesa;
    private int ataque;
    /**
     * Método construtor de Item.
     */
    Item(int defesa,int ataque){
        this.defesa = defesa;
        this.ataque = ataque;
    }

    /**
     * Método que retorna defesa de item.
     * @return int - retorna defesa de item.
     */
    public int getDefesa() {
        return defesa;
    }

    /**
     *Método que retorna ataque de item.
     * @return  int - retorna ataque de item.
     */
    public int getAtaque() {
        return ataque;
    }
    /**
     * Método que define defesa de item.
     * @param  defesa int - defesa de item.
     */
    void setDefesa(int defesa) {
        this.defesa = defesa;
    }
}
