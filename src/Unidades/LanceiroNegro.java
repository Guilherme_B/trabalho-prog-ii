package Unidades;

import Exercitos.Dados;

/**
 *Classe de Lanceiro Negro.
 * @author Augusto Garcia e Éric Peralta 
 */
public class LanceiroNegro extends Unidade{

    /**
     *Método construtor de Lanceiro Negro.
     */
    public LanceiroNegro(){
       this.arma = new Item(100,100);
   }
   
    /**
     *Método que executa o ataque especial.
     * @param inimigo Unidade - inimigo que receberá o dano.
     */
    @Override
   public void ataqueEspecial(Unidade inimigo){;
       if(r.nextInt(10)>3 && this.arma.getDefesa()>0){
           if(inimigo.arma.getDefesa()>0){
               inimigo.arma.setDefesa(0);
           }
           else{
               inimigo.setVida((inimigo.getVida()/3));
           }
           
           this.arma.setDefesa(0);
           this.setAtaque(this.getAtaque()*2);
       }
   }
   
    /**
     *Método que define vida e ataque de lanceiro negro.
     */
    @Override
    public void setTudo(){
        this.ataque = Dados.getLanAtk();
        this.vida = Dados.getLanVid();
    }
}
