package Unidades;

import Exercitos.Dados;
import java.util.ArrayList;

/**
 *Classe de cavalerio.
 * @author Augusto Garcia e Éric Peralta 
 */
public class Padre extends Unidade{

    /**
     *Método construtor de padre.
     */
    public Padre(){
       this.arma = new Item(100,100);
   }
   
    /**
     *Método que executa o ataque especial.
     * @param inimigo Unidade - inimigo que receberá o dano.
     */
    @Override
   public void ataqueEspecial(Unidade inimigo){
       if(inimigo instanceof Padre){
           this.setAtaque(this.getAtaque()*2);
       }
   }
   
    /**
     *Método que define vida e ataque de padre.
     */
    @Override
    public void setTudo(){
        this.ataque = Dados.getPadAtk();
        this.vida = Dados.getPadVid();
    }
}
