/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetogame;

import Exercitos.*;
import Unidades.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
/**
 * Classe controller de batalha.
 * @author Augusto Garcia e Éric Peralta. 
 */
public class BatalhatelaController implements Initializable {
    
    //Aliado
    @FXML private Label genAli;
    @FXML private Label cavAli;
    @FXML private Label lanAli;
    @FXML private Label artAli;
    @FXML private Label padAli;
    @FXML private Label aliado;
    @FXML private Label morAli;
    
    //Inimigo
    @FXML private Label genIni;
    @FXML private Label cavIni;
    @FXML private Label lanIni;
    @FXML private Label artIni;
    @FXML private Label padIni;
    @FXML private Label inimigo;
    @FXML private Label morIni;
    
    //Exercitos
    private ArrayList<Unidade> aliados = new ArrayList<Unidade>();
    private ArrayList<Unidade> inimigos;
    
    //Soldados mortos
    private int nAli=0, nIni=0;
    
    //Variável de sortear
    private Random r = new Random();
    /**
     * Método que inicia junto a tela de batalha.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        genAli.setText("Generais: " + Usuario.getGenQunt());
        cavAli.setText("Cavaleiros: " + Usuario.getCavQunt());
        lanAli.setText("Lançeiros: " + Usuario.getLanQunt());
        artAli.setText("Infantarias: " + Usuario.getInfQunt());
        padAli.setText("Padres: " + Usuario.getPadQunt());
        
        colocaTextosInimigos();
        
        iniciaAliados();
        iniciaInimigos();
        
        batalha();
        
        morAli.setText("Mortos: " + this.nAli);
        morIni.setText("Mortos: " + this.nIni);
        
        if(inimigos.isEmpty()){
            aliado.setText("Venceu!");
            inimigo.setText("Perdeu!");
            Usuario.Bonus(3000);
        }
        
        else{
            aliado.setText("Perdeu!");
            inimigo.setText("Venceu!");
            Usuario.setDinheiro(2500);
        }
        inimigos = null;
        aliados = null;
    }
    
    @FXML
    private void VoltarButton(){
        ProjetoGame.trocaTela("TelaInicial.fxml");
    }
    
    private void iniciaAliados(){
        for(int i=0;i<Usuario.getCavQunt();i++){
            aliados.add(new Cavaleiro());
        }
        
        for(int i=0;i<Usuario.getGenQunt();i++){
            aliados.add(new General());
        }
        
        for(int i=0;i<Usuario.getLanQunt();i++){
            aliados.add(new LanceiroNegro());
        }
        
        for(int i=0;i<Usuario.getInfQunt();i++){
            aliados.add(new Infantaria());
        }
        
        for(int i=0;i<Usuario.getPadQunt();i++){
            aliados.add(new Padre());
        }
        
        int p = Usuario.getCavQunt() + 
                Usuario.getGenQunt() +
                Usuario.getInfQunt() +
                Usuario.getLanQunt() +
                Usuario.getPadQunt();
        
        for(int i=0;i<p;i++){
            aliados.get(i).setTudo();
        }
    }
    /**
     * Método que inicia inimigos.
     */
    private void iniciaInimigos(){
       this.inimigos=EscolhaBatalhaController.getInimigos();
       for(int i=0;i<inimigos.size();i++)
           inimigos.get(i).setTudo();
    }
    /**
     * Método que executa a batalha.
     */
    private void batalha(){
        int ali, ini;
        boolean aliVivo, iniVivo;
        
        do{
           ali = r.nextInt(aliados.size());
           ini = r.nextInt(inimigos.size());
           aliVivo = true;
           iniVivo = true;
            //Batalha em si*************************************************************************************************************************************
           do{
                if(r.nextInt(2) == 1){//Aliados atacam primeiro
                    inimigos.get(ini).sofreDano(aliados.get(ali).getAtaque());
                    aliados.get(ali).ataqueEspecial(inimigos.get(ini));
                    
                        
                    if(inimigos.get(ini).getVida() > 0){
                        aliados.get(ali).sofreDano(inimigos.get(ini).getAtaque());
                        inimigos.get(ini).ataqueEspecial(aliados.get(ali));
                    }
                }
                
                else{//inimigos atacam primeiro
                    aliados.get(ali).sofreDano(inimigos.get(ini).getAtaque());
                    inimigos.get(ini).ataqueEspecial(aliados.get(ali));
                    
                    if(aliados.get(ali).getVida() > 0){
                        inimigos.get(ini).sofreDano(aliados.get(ali).getAtaque());
                        aliados.get(ali).ataqueEspecial(inimigos.get(ini));
                    }
                }
                
                if(aliados.get(ali).getVida()<=0){
                    if(aliados.get(ali) instanceof Cavaleiro){
                        Usuario.rmCav();
                    }
                    else if(aliados.get(ali) instanceof Padre){
                        Usuario.rmPad();
                    }
                    else if(aliados.get(ali) instanceof LanceiroNegro){
                        Usuario.rmLan();
                    }
                    else if(aliados.get(ali) instanceof General){
                        Usuario.rmGen();
                    }
                    else{
                        Usuario.rmInf();
                    }
                    aliados.remove(ali);
                    nAli++;
                    aliVivo = false;
                }
                
                if(inimigos.get(ini).getVida()<=0){
                    inimigos.remove(ini);
                    nIni++;
                    iniVivo = false;
                }
           }while(aliVivo && iniVivo);
        }while(!(aliados.isEmpty()||inimigos.isEmpty()));
    }
    /**
     * Método que imprime as informações de inimigo na tela.
     */
    private void colocaTextosInimigos(){
        int i = EscolhaBatalhaController.getP();
        if(i == 1){
            genIni.setText("Generais: " + ArroioGrande.getQuntGen());
            cavIni.setText("Cavaleiros: " + ArroioGrande.getQuntCav());
            lanIni.setText("Lançeiros: " + ArroioGrande.getQuntLan());
            artIni.setText("Infantarias: " + ArroioGrande.getQuntInf());
            padIni.setText("Padres: " + ArroioGrande.getQuntPad());
        }
        
        else if(i == 2){
            genIni.setText("Generais: " + Fanfa.getQuntGen());
            cavIni.setText("Cavaleiros: " + Fanfa.getQuntCav());
            lanIni.setText("Lançeiros: " + Fanfa.getQuntLan());
            artIni.setText("Infantarias: " + Fanfa.getQuntInf());
            padIni.setText("Padres: " + Fanfa.getQuntPad());
        }
        
        else if(i == 3){
            genIni.setText("Generais: " + PonteDaAzenha.getQuntGen());
            cavIni.setText("Cavaleiros: " + PonteDaAzenha.getQuntCav());
            lanIni.setText("Lançeiros: " + PonteDaAzenha.getQuntLan());
            artIni.setText("Infantarias: " + PonteDaAzenha.getQuntInf());
            padIni.setText("Padres: " + PonteDaAzenha.getQuntPad());
        }
        
        else{
            genIni.setText("Generais: " + RevoltaNegra.getQuntGen());
            cavIni.setText("Cavaleiros: " + RevoltaNegra.getQuntCav());
            lanIni.setText("Lançeiros: " + RevoltaNegra.getQuntLan());
            artIni.setText("Infantarias: " + RevoltaNegra.getQuntInf());
            padIni.setText("Padres: " + RevoltaNegra.getQuntPad());
        }
    }
}
