package projetogame;

import Exercitos.Dados;
import Exercitos.Usuario;
import Unidades.General;
import javafx.scene.control.TextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 *Classe controller da tela de general.
 * @author Augusto Garcia e Éric peralta
 */
public class GeneralController implements Initializable {
    @FXML private Label dindin;
    @FXML private TextField qunt;
    @FXML private Label aviso;
    @FXML private Label cust;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dindin.setText("R$ " + Usuario.getDinheiro());
    }
    @FXML
    private void calcust(){
        cust.setText(String.valueOf(Dados.getGenCust()*Integer.parseInt(qunt.getText())));
    }
    @FXML
    private void VoltarButton(){
        int val = Integer.parseInt(qunt.getText());
        if((Dados.getGenCust()*val) <= Usuario.getDinheiro()){
            Usuario.diminui(val*Dados.getGenCust());
            for(int i=0;i<val;i++){
                Usuario.addGen();
            }
            ProjetoGame.trocaTela("TelaInicial.fxml");
        }
        else{
            aviso.setText("Não a dinheiro suficiente");
        }
       
    }
    
    @FXML
    private void maisVida(){
        if(Usuario.getDinheiro()>=10){
            Dados.setGenVid(Dados.getGenVid() + 10);
            Usuario.diminui(10);
            atualizaDinheiro();
        }
    }
    
    @FXML
    private void maisAtaque(){
        if(Usuario.getDinheiro()>=10){
            Dados.setGenAtk(Dados.getGenAtk() + 10);
            Usuario.diminui(10);
            atualizaDinheiro();
        }
    }
    
    private void atualizaDinheiro(){
        dindin.setText("R$ "+ Usuario.getDinheiro());
    }
}
