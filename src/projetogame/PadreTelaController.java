/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetogame;

import Exercitos.Dados;
import Exercitos.Usuario;
import Unidades.Infantaria;
import javafx.scene.control.TextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import Unidades.Padre;
import javafx.scene.control.Label;

/**
 * Classe controller da tela de padre.
 * @author Augusto Garcia e Éric Peralta
 */
public class PadreTelaController implements Initializable {

    @FXML private Label dindin;
    @FXML private TextField qunt;
    @FXML private Label aviso;
    @FXML private Label cust;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dindin.setText("R$ " + Usuario.getDinheiro());
    }
    @FXML
    private void calcust(){
        cust.setText(String.valueOf(Dados.getPadCust()*Integer.parseInt(qunt.getText())));
    }
    @FXML
    private void VoltarButton(){
        int val = Integer.parseInt(qunt.getText());
        if((Dados.getPadCust()*val) <= Usuario.getDinheiro()){
            Usuario.diminui(val*Dados.getPadCust());
            for(int i=0;i<val;i++){
                Usuario.addPad();
            }
            ProjetoGame.trocaTela("TelaInicial.fxml");
        }
        
       else{
            aviso.setText("Não a dinheiro suficiente");
        }
    }
    
    @FXML
    private void maisVida(){
        if(Usuario.getDinheiro()>=10){
            Dados.setPadVid(Dados.getPadVid() + 10);
            Usuario.diminui(10);
            atualizaDinheiro();
        }
    }
    
    @FXML
    private void maisAtaque(){
        if(Usuario.getDinheiro()>=10){
            Dados.setPadAtk(Dados.getPadAtk() + 10);
            Usuario.diminui(10);
            atualizaDinheiro();
        }
    }
    
    private void atualizaDinheiro(){
        dindin.setText("R$ "+ Usuario.getDinheiro());
    }
}
